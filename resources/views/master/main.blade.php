<!DOCTYPE html>
<html>
<head>
  @include('partial.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  @include('partial.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('partial.sidebar')
  <!-- .Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  @yield('content')
  <!-- Content Wrapper. Contains page content -->

  @include('partial.footer')

</div>

<!-- JS -->
@include('partial.jsfooter');
<!-- JS -->

</body>
</html>
