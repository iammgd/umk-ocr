@extends("master.main")

@section("title","Dashboard")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <!-- Small boxes (Stat box) -->
            Halo, {{Auth::user()->name}}.
            @if(Auth::user()->level_akses==2)
            <br/>
            <center><font style="font-family: arial;color:#008577;font-size:14pt">Rp{{$dept['total_terpakai']}}</font>/<font style="font-family: arial;">Rp{{$dept['total_budget']}}</font></center><br>
            <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            @endif
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<script>
var terpakai = <?php echo json_encode($dept['persen_terpakai']); ?>;
var sisa = <?php echo json_encode($dept['persen_sisa']); ?>;

$(function () {
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData = {
        labels: [
            'Terpakai', 
            'Sisa'
        ],
        datasets: [{
            data: [terpakai,sisa],
            backgroundColor : ['#f39c12', '#f56954'],
        }]
    }
    var donutOptions = {
        maintainAspectRatio : false,
        responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(donutChartCanvas, {
        type: 'doughnut',
        data: donutData,
        options: donutOptions      
    })
});
</script>
@endsection