@extends("master.main")

@section("title","Tolak Dokumen")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tolak Dokumen</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('user.document.index')}}">Daftar Pengajuan</a></li>
                        <li class="breadcrumb-item active">Tolak Dokumen</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <form id="form-tolak-document" method="POST" action="{{route('user.persetujuan.tolak')}}">
                        @csrf
                        <input type="hidden" name="doc_id" value="{{$doc_id}}">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Alasan Penolakan</label>
                                        <Textarea class="form-control{{ $errors->has('reject_notes') ? ' is-invalid' : '' }}" name="reject_notes" placeholder="Masukkan alasan penolakan" required="true" autofocus></Textarea>
                                        @if ($errors->has('reject_notes'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('reject_notes') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                        <div class="card-footer">
                            <button id="btn-tolak-document" class="btn pull-right" style="background-color: #008577;color:white;margin-left:5px;">Simpan</button>
                            <a href="{{url()->previous()}}" class="btn pull-right btn-primary">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    $('#btn-tolak-document').on('click', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        swal({
            title: 'Apakah Anda yakin ingin menolak dokumen ini?',
            text: 'Aksi pada dokumen tidak dapat dibatalkan.',
            type: 'warning',
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya!',
            showCancelButton: true,
            cancelButtonText: 'Batal!',
            html: false,
            preConfirm: function() {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 50);
                });
            }
        }).then(function(result){
            if (result.value) {
                // form action delete
                document.getElementById('form-tolak-document').submit();
                    
            }
        });
    });

</script>


@endsection