@extends("master.main")

@section("title","Detail Dokumen")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Dokumen</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('user.document.index')}}">Daftar Pengajuan</a></li>
                        <li class="breadcrumb-item active">Detail Dokumen</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if($doc->status_text=="Sudah Diajukan")
                        <button id="btn-terima-document" class="btn pull-right" style="margin-left:10px;background-color: #008577;color:white;">TERIMA</button>
                        <a href="{{route('user.persetujuan.tolak.html', [$doc->id])}}" class="btn pull-right" style="margin-left:10px;background-color: #E12525;color:white;">TOLAK</a>
                    @elseif($doc->status_text=="Ditolak")
                        <a href="{{route('user.persetujuan.tolak.html', [$doc->id])}}" class="btn pull-right" style="margin-left:10px;background-color: #008577;color:white;">Ubah Alasan Penolakan</a>
                    @endif
                    <a href="{{route('document.print', [$doc->id])}}" class="btn btn-primary pull-right" style="margin-left:10px;">PRINT</a>
                </div>
            </div>
            <br>
            <!-- /.row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <font class="pull-left" style="font-size:14pt;"><b>Informasi Dokumen</b></font>
                        </div>
                        <div class="card-body">
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Document No</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$doc->document_no}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{!!$doc->status_color!!}</font>
                                        @if($doc->status_text=="Ditolak")
                                            <br><font style="margin-left: 10px;font-family: arial;">alasan: {{$doc->reject_notes}}</font>
                                        @elseif($doc->umur_settle!="")
                                            <br><font style="margin-left: 10px;font-family: arial;">Terlambat {{$doc->umur_settle}} hari</font>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Cost Center</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$doc->cost_center_name}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Kode Project</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$doc->project->kode_project}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Project</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$doc->project->name}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Activity</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$doc->activity->name}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$doc->start_date}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>End Date</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$doc->end_date}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Total</label>
                                        <br><font style="margin-left: 10px;font-family: arial;font-size:14pt;"><b>Rp{{$doc->amount_request_rp}}</b></font>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-sm-6">
                    @if($doc->status_text=="Diterima" || $doc->status_text=="Settled")
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <font class="pull-left" style="font-size:14pt;"><b>Settlement</b></font>
                                    @if($doc->status_text=="Diterima")<a href="#" data-toggle="modal" data-target="#modal-settleadd" class="btn pull-right" style="margin-left:10px;background-color: #008577;color:white;">Tambah File Settlement</a>@endif
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <b><font class="pull-right" style="font-family: arial;">Total Perbedaan: {{$doc->amount_difference_rp}}</font></b><br>
                                    </div>
                                    <!-- /.row -->
                                    <div class="card-body table-responsive p-0">
                                        <table class="table table-hover">
                                            <tr>
                                                <th>No.</th>
                                                <th>Keterangan</th>
                                                <th>File</th>
                                                <th>Total</th>
                                                @if($doc->status_text=="Diterima")<th>Aksi</th>@endif
                                            </tr>
                                            @php $i=1 @endphp
                                            @forelse ($doc->settlements as $settle)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $settle->keterangan }}</td>
                                                <td>{{ $settle->nama_file }}</td>
                                                <td>Rp{{ $settle->amount_rp }}</td>
                                                @if($doc->status_text=="Diterima")
                                                    <td><button onclick="deleteSettle({{$settle->id}});" class="btn btn-sm" style="background-color: #E12525;color:white;">HAPUS</button></td>
                                                    <form id="form-hapus-settle{{$settle->id}}" method="POST" action="{{route('user.settle.delete', [$settle->id])}}">
                                                        @csrf
                                                        {{ method_field('DELETE') }}
                                                    </form>
                                                @endif
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="5"><center><i>Tidak ada data settlement.</i></center></td>
                                            </tr>
                                            @endforelse
                                            <tr>
                                                <td colspan="3" style="text-align:right"><b>Total</b></td>
                                                <td><b>Rp{{number_format($doc->settlements->sum('amount'),2)}}</b></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <font class="pull-left" style="font-size:14pt;"><b>Data Pengaju</b></font>
                                </div>
                                <div class="card-body">
                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <br><font style="margin-left: 10px;font-family: arial;">{{$doc->user->name}}</font>
                                            </div>
                                            <div class="form-group">
                                                <label>Departemen</label>
                                                <br><font style="margin-left: 10px;font-family: arial;">{{$doc->user->departement->name}}</font>
                                            </div>
                                            <div class="form-group">
                                                <label>Jabatan</label>
                                                <br><font style="margin-left: 10px;font-family: arial;">L{{$doc->user->urutan}}</font>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <font class="pull-left" style="font-size:14pt;"><b>Daftar Item</b></font>
                                </div>
                                <div class="card-body">
                                    <!-- /.row -->
                                    <div class="card-body table-responsive p-0">
                                        <table class="table table-hover">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Notes</th>
                                                <th>Qty</th>
                                                <th>Unit</th>
                                                <th>Nilai/Unit</th>
                                                <th>Nilai Total</th>
                                            </tr>
                                            @php $i=1 @endphp
                                            @forelse ($doc->items as $item)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->notes }}</td>
                                                <td>{{ $item->quantity }}</td>
                                                <td>{{ $item->unit }}</td>
                                                <td>Rp{{ $item->unit_price_rp }}</td>
                                                <td>Rp{{ $item->total_price_rp }}</td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="8"><center><i>Tidak ada data item.</i></center></td>
                                            </tr>
                                            @endforelse
                                            <tr>
                                                <td colspan="6" style="text-align:right"><b>Total</b></td>
                                                <td><b>Rp{{number_format($doc->items->sum('total_price'),2)}}</b></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>


@include('module.user.document.itemadd')

<form id="form-terima-document" method="POST" action="{{route('user.persetujuan.terima', [$doc])}}">
    @csrf
</form>

<script type="text/javascript">

    var rupiah = document.getElementById('rupiah');
    rupiah.addEventListener('keyup', function(evt){
        var n = parseInt(this.value.replace(/\D/g,''),10);
        rupiah.value = n.toLocaleString();
    }, false);

    $('#btn-terima-document').on('click', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        swal({
            title: 'Apakah Anda yakin ingin menerima dokumen ini?',
            text: 'Aksi pada dokumen tidak dapat dibatalkan.',
            type: 'warning',
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya!',
            showCancelButton: true,
            cancelButtonText: 'Batal!',
            html: false,
            preConfirm: function() {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 50);
                });
            }
        }).then(function(result){
            if (result.value) {
                // form action delete
                document.getElementById('form-terima-document').submit();
                    
            }
        });
    });

</script>

@endsection