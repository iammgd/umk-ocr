<div id="modal-itemadd" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('user.item.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">Tambah Item</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>            
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="document_id" value="{{$doc->id}}">
                        <div class="form-group">
                            <label>Nama Item</label>
                            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" required="true" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Qty</label>
                            <input type="number" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" required="true" value="{{ old('quantity') }}">
                            @if ($errors->has('quantity'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('quantity') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Nama Unit</label>
                            <input type="text" class="form-control{{ $errors->has('unit') ? ' is-invalid' : '' }}" name="unit" required="true" value="{{ old('unit') }}">
                            @if ($errors->has('unit'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('unit') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Harga Per Unit</label>
                            <input type="text" id="rupiah" class="form-control{{ $errors->has('unit_price') ? ' is-invalid' : '' }}" name="unit_price" required="true" value="{{ old('unit_price') }}">
                            @if ($errors->has('unit_price'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('unit_price') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Catatan</label>
                            <Textarea class="form-control{{ $errors->has('notes') ? ' is-invalid' : '' }}" name="notes"></Textarea>
                            @if ($errors->has('notes'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('notes') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>File</label>
                            <input type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="file">
                            @if ($errors->has('file'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
            <div class="modal-footer justifiy-content-between">
                <button type="submit" class="btn btn-primary pull-right">Tambah</button>
            </div>
            </form>
        </div>
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->