<div id="modal-settleadd" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('user.settle.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">Tambah File Settlement</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>            
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="document_id" value="{{$doc->id}}">
                        <div class="form-group">
                            <label>Jumlah/Amount</label>
                            <input type="text" id="rupiahsettle" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" required="true" value="{{ old('amount') }}">
                            @if ($errors->has('amount'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <Textarea class="form-control{{ $errors->has('keterangan') ? ' is-invalid' : '' }}" name="keterangan">{{ old('keterangan') }}</Textarea>
                            @if ($errors->has('keterangan'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('keterangan') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>File</label>
                            <input type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="file">
                            @if ($errors->has('file'))
                                <span class="invalid-feedback" style="color:red" role="alert">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
            <div class="modal-footer justifiy-content-between">
                <button type="submit" class="btn btn-primary pull-right">Tambah</button>
            </div>
            </form>
        </div>
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->