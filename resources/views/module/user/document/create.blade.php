@extends("master.main")

@section("title","Ajukan Dokumen")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ajukan Dokumen</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('user.document.index')}}">Daftar Dokumen</a></li>
                        <li class="breadcrumb-item active">Ajukan Dokumen</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form action="{{route('user.document.store')}}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Cost Center</label>
                                        <select class="cost form-control select2" name="cost_center_id" id="cost_center_id" style="width:100%;">
                                        @foreach($cost_centers as $cost_center)
                                            <option value="{{$cost_center->id}}">{{$cost_center->name}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Project</label>
                                        <select class="project form-control select2" name="project_id" id="project_id" style="width:100%;">
                                        
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Activity</label>
                                        <select class="form-control select2" name="activity_id" style="width:100%;">
                                        @foreach($activities as $activity)
                                            <option value="{{$activity->id}}">{{$activity->name}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <Textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Insert Description" autofocus></Textarea>
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Amount Request</label>
                                        <input type="text" id="rupiah" class="form-control{{ $errors->has('amount_request') ? ' is-invalid' : '' }}" name="amount_request" placeholder="X.XXX.XXX" required="true" value="{{ old('amount_request') }}">
                                        @if ($errors->has('amount_request'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('amount_request') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }} datepicker" name="start_date" required="true" value="{{ old('start_date') }}">
                                            @if ($errors->has('start_date'))
                                                <span class="invalid-feedback" style="color:red" role="alert">
                                                    <strong>{{ $errors->first('start_date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>End Date</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }} datepicker" name="end_date" required="true" value="{{ old('end_date') }}">
                                            @if ($errors->has('end_date'))
                                                <span class="invalid-feedback" style="color:red" role="alert">
                                                    <strong>{{ $errors->first('start_date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Request Note</label>
                                        <Textarea class="form-control{{ $errors->has('request_note') ? ' is-invalid' : '' }}" name="request_note" placeholder="Note..."></Textarea>
                                        @if ($errors->has('request_note'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('request_note') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Reason 1</label>
                                        <input type="text" class="form-control{{ $errors->has('reason_satu') ? ' is-invalid' : '' }}" name="reason_satu" value="{{ old('reason_satu') }}">
                                        @if ($errors->has('reason_satu'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('reason_satu') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Reason 2</label>
                                        <input type="text" class="form-control{{ $errors->has('reason_dua') ? ' is-invalid' : '' }}" name="reason_dua" value="{{ old('reason_dua') }}">
                                        @if ($errors->has('reason_dua'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('reason_dua') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>  
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn pull-right" style="background-color: #008577;color:white;margin-left:5px;">Ajukan</button>
                            <a href="{{url()->previous()}}" class="btn pull-right btn-primary">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    var $project_id = $('.project');
    var $cost_center_id = $(".cost");

    $cost_center_id.on('change', function() {
        $.ajax({
            url:"{{url('projects')}}" + '/' + $cost_center_id.val(), // if you say $(this) here it will refer to the ajax call not $('.company2')
            type:'GET',
            success:function(data) {
                $project_id.empty();
                $.each(data, function(value, key) {
                    $project_id.append($("<option></option>").attr("value", value).text(key)); // name refers to the objects value when you do you ->lists('name', 'id') in laravel
                });
                $project_id.select2(); //reload the list and select the first option
            }
        });
    }).trigger('change');

    var rupiah = document.getElementById('rupiah');
    rupiah.addEventListener('keyup', function(evt){
        var n = parseInt(this.value.replace(/\D/g,''),10);
        rupiah.value = n.toLocaleString();
    }, false);

    $(function(){
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
    });
</script>

@endsection