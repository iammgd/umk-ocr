@extends("master.main")

@section("title","Daftar Dokumen")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Dokumen</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Daftar Dokumen</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- pencarian -->
            <div class="row">
                <div class="col-12">
                    <div class="card collapsed-card">
                        <div class="card-header">
                            <h3 class="card-title">Pencarian</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{route('document.search')}}" method="GET">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Document No</label>
                                            <input type="text" class="form-control" name="doc_no"  @if(isset($doc_no)) value="{{$doc_no}}" @endif/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Start Date (from.. to..)</label>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control datepicker" name="start_date_from" @if(isset($start_date_from)) value="{{$start_date_from}}" @endif placeholder="yyyy-mm-dd">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control datepicker" name="start_date_to" @if(isset($start_date_to)) value="{{$start_date_to}}" @endif placeholder="yyyy-mm-dd">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control select2" name="status" style="width:100%;">
                                                <option value="all" @if(isset($status) && $status=='all') selected="true" @endif>Semua Status</option>
                                                <option value="0" @if(isset($status) && $status==0) selected="true" @endif>Belum Diajukan</option>
                                                <option value="1" @if(isset($status) && $status==1) selected="true" @endif>Sudah Diajukan</option>
                                                <option value="2" @if(isset($status) && $status==2) selected="true" @endif>Ditolak</option>
                                                <option value="3" @if(isset($status) && $status==3) selected="true" @endif>Diterima</option>
                                                <option value="4" @if(isset($status) && $status==4) selected="true" @endif>Close</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <!--form close-->
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn pull-right" style="margin-left:4px;background-color: #008577;color:white;">
                                <i class="fa fa-search"></i> &nbsp; Cari
                            </button>
                            <a href="{{route('user.document.index')}}" class="btn pull-right btn-warning" style="color:white;">Reset</a>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('user.document.create')}}" class="btn pull-right" style="margin-left:4px;background-color: #008577;color:white;">
                                <i class="fa fa-plus"></i> &nbsp; Ajukan Dokumen
                            </a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tr>
                                    <th>No.</th>
                                    <th>Document No</th>
                                    <th>Status</th>
                                    <th>Project</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Amount</th>
                                    <th>Aksi</th>
                                </tr>
                                @forelse ($docs as  $keys => $doc)
                                <tr>
                                    <td>{{ $docs->firstItem() + $keys}}</td>
                                    <td>{{ $doc->document_no }}</td>
                                    <td>{!! $doc->status_color !!}</td>
                                    <td>{{ $doc->project_name }}</td>
                                    <td>{{ $doc->start_date }}</td>
                                    <td>{{ $doc->end_date }}</td>
                                    <td>Rp{{ $doc->amount_request_rp }}</td>
                                    <td><a href="{{route('user.document.detail', [$doc->id])}}" class="btn btn-sm" style="margin-left:4px;background-color: #008577;color:white;">Detail</a></td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10"><center><i>Tidak ada data document yang ditemukan.</i></center></td>
                                </tr>
                                @endforelse
                            </table>
                            <ul style="margin-right: 15px;margin-top: 15px;" class="blue pull-right">
                                {{ $docs->appends(request()->input())->links() }}
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    $(function(){
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
    });
</script>

@endsection