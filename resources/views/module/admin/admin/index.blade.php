@extends("master.main")

@section("title","Daftar Admin")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Admin</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Daftar Admin</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-header">
                            <a href="{{route('admin.create')}}" class="btn pull-right" style="margin-left:4px;background-color: #008577;color:white;">
                                <i class="fa fa-plus"></i> &nbsp; Tambah Admin
                            </a>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                                @forelse ($admins as  $keys => $admin)
                                <tr>
                                    <td>{{ $admins->firstItem() + $keys}}</td>
                                    <td>{{ $admin->name }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td><a href="{{route('admin.detail', [$admin->id])}}" class="btn btn-sm" style="margin-left:4px;background-color: #008577;color:white;">Detail</a></td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6"><center><i>Tidak ada data admin yang ditemukan.</i></center></td>
                                </tr>
                                @endforelse
                            </table>
                            <ul style="margin-right: 15px;margin-top: 15px;" class="blue pull-right">
                                {{ $admins->appends(request()->input())->links() }}
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection