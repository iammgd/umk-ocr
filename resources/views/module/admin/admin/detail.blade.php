@extends("master.main")

@section("title","Detail Admin")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Admin</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Daftar Admin</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama Admin</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$admin->name}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$admin->email}}</font>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                        <div class="card-footer">
                            <button id="btn-hapus-admin" class="btn pull-left" style="background-color:#E12525;color:white">Hapus</button>
                            <a href="{{route('admin.edit', [$admin])}}" class="btn pull-right" style="background-color: #008577;color:white;margin-left: 5px;">Ubah</a>
                            <a href="{{route('admin.index')}}" class="btn pull-right btn-primary">Kembali</a>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<form id="form-hapus-admin" method="POST" action="{{route('admin.delete', [$admin])}}">
    @csrf
    {{ method_field('DELETE') }}
</form>

<script type="text/javascript">
    $('#btn-hapus-admin').on('click', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        swal({
            title: 'Apakah Anda yakin?',
            text: 'Data yang sudah dihapus tidak dapat dikembalikan lagi.',
            type: 'warning',
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya!',
            showCancelButton: true,
            cancelButtonText: 'Batal!',
            html: false,
            preConfirm: function() {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 50);
                });
            }
        }).then(function(result){
            if (result.value) {
                // form action delete
                document.getElementById('form-hapus-admin').submit();
                    
            }
        });
    });
</script>

@endsection