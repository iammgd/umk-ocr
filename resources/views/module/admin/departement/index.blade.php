@extends("master.main")

@section("title","Daftar Departement")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Departement</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Daftar Departement</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{url('departement/create')}}" class="btn pull-right" style="margin-left:4px;background-color: #008577;color:white;">
                                <i class="fa fa-plus"></i> &nbsp; Tambah Departement
                            </a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tr>
                                    <th>No.</th>
                                    <th>ID Departement</th>
                                    <th>Nama Departement</th>
                                    <th>Aksi</th>
                                </tr>
                                @forelse ($deps as  $keys => $dep)
                                <tr>
                                    <td>{{ $deps->firstItem() + $keys}}</td>
                                    <td>{{ $dep->id }}</td>
                                    <td>{{ $dep->name }}</td>
                                    <td><a href="/" class="btn btn-sm" style="margin-left:4px;background-color: #008577;color:white;">Detail</a></td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5"><center><i>Tidak ada data departement.</i></center></td>
                                </tr>
                                @endforelse
                            </table>
                            <ul style="margin-right: 15px;margin-top: 15px;" class="red pagination pagination-md pull-right">
                                {{ $deps->appends(request()->input())->links() }}
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection