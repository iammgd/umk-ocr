@extends("master.main")

@section("title","Ubah Karyawan")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ubah Karyawan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('karyawan.index')}}">Daftar Karyawan</a></li>
                        <li class="breadcrumb-item active">Ubah Karyawan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <form action="{{route('karyawan.update', [$karyawan])}}" method="POST">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama Karyawan</label>
                                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Masukkan nama karyawan" required="true" value="{{$karyawan->name}}" autofocus>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Masukkan email karyawan" required="true" value="{{$karyawan->email}}" autofocus>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Departement</label>
                                        <select class="form-control select2" name="departement_id" style="width:100%;">
                                            @foreach($departements as $departement)
                                                <option value="{{$departement->id}}" @if($karyawan->departement_id==$departement->id) selected="true" @endif>{{$departement->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Level</label>
                                        <select class="form-control" name="urutan">
                                            <option value="3" @if($karyawan->urutan==3) selected="true" @endif>L3</option>
                                            <option value="2" @if($karyawan->urutan==2) selected="true" @endif>L2</option>
                                            <option value="1" @if($karyawan->urutan==1) selected="true" @endif>L1</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Masukkan password apabila ingin diubah" autofocus>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" style="color:red" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn pull-right" style="background-color: #008577;color:white;margin-left:5px;">Simpan</button>
                            <a href="{{url()->previous()}}" class="btn pull-right btn-primary">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection