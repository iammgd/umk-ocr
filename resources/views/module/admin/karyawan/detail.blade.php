@extends("master.main")

@section("title","Detail Karyawan")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Karyawan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('karyawan.index')}}">Daftar Karyawan</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama Karyawan</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$karyawan->name}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$karyawan->email}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Departement</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">{{$karyawan->departement->name}}</font>
                                    </div>
                                    <div class="form-group">
                                        <label>Level</label>
                                        <br><font style="margin-left: 10px;font-family: arial;">L{{$karyawan->urutan}}</font>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="card-footer">
                            <button id="btn-hapus-karyawan" class="btn pull-left" style="background-color:#E12525;color:white">Hapus</button>
                            <a href="{{route('karyawan.edit', [$karyawan])}}" class="btn pull-right" style="background-color: #008577;color:white;margin-left: 5px;">Ubah</a>
                            <a href="{{route('karyawan.index')}}" class="btn pull-right btn-primary">Kembali</a>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<form id="form-hapus-karyawan" method="POST" action="{{route('karyawan.delete', [$karyawan])}}">
    @csrf
    {{ method_field('DELETE') }}
</form>

<script type="text/javascript">
    $('#btn-hapus-karyawan').on('click', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        swal({
            title: 'Apakah Anda yakin?',
            text: 'Data yang sudah dihapus tidak dapat dikembalikan lagi.',
            type: 'warning',
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya!',
            showCancelButton: true,
            cancelButtonText: 'Batal!',
            html: false,
            preConfirm: function() {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 50);
                });
            }
        }).then(function(result){
            if (result.value) {
                // form action delete
                document.getElementById('form-hapus-karyawan').submit();
                    
            }
        });
    });
</script>

@endsection