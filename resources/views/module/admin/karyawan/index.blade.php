@extends("master.main")

@section("title","Daftar Karyawan")

@section("content")

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Karyawan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Daftar Karyawan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('partial.alert')
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-header">
                            <a href="{{route('karyawan.create')}}" class="btn pull-right" style="margin-left:4px;background-color: #008577;color:white;">
                                <i class="fa fa-plus"></i> &nbsp; Tambah Karyawan
                            </a>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Departemen</th>
                                    <th>Hak Akses</th>
                                    <th>Aksi</th>
                                </tr>
                                @forelse ($karyawans as  $keys => $karyawan)
                                <tr>
                                    <td>{{ $karyawans->firstItem() + $keys}}</td>
                                    <td>{{ $karyawan->name }}</td>
                                    <td>{{ $karyawan->email }}</td>
                                    <td>{{ $karyawan->departement->name }}</td>
                                    <td>L{{ $karyawan->urutan }}</td>
                                    <td><a href="{{route('karyawan.detail', [$karyawan->id])}}" class="btn btn-sm" style="margin-left:4px;background-color: #008577;color:white;">Detail</a></td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6"><center><i>Tidak ada data karyawan yang ditemukan.</i></center></td>
                                </tr>
                                @endforelse
                            </table>
                            <ul style="margin-right: 15px;margin-top: 15px;" class="blue pull-right">
                                {{ $karyawans->appends(request()->input())->links() }}
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

{{-- Modal Map --}}
<div id="modal-import" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('karyawan.import')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">Pilih File Excel Karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>            
            </div>
            <div class="modal-body">
                <div class="form-group">
                    Urutan kolom pada setiap baris harus mengikuti aturan berikut:
                    <ol>
                        <li>Nama Karyawan</li>
                        <li>Email</li>
                        <li>Password</li>
                        <li>ID Fungsi</li>
                        <li>Level (1: admin, 2: karyawan)</li>
                    </ol>
                    *Import file excel tanpa header (Baris pertama pada excel adalah data pertama, bukan header).
                    <a href="{{route('karyawan.import.example')}}" class="btn btn-sm btn-primary mr-2"><i class="fa fa-excel-o"></i> Download Contoh/Template</a>
                </div>
                <div class="form-group">
                    <label>File Excel</label>
                    <input type="file" class="form-control{{ $errors->has('file_excel') ? ' is-invalid' : '' }}" name="file_excel">
                    @if ($errors->has('file_excel'))
                        <span class="invalid-feedback" style="color:red" role="alert">
                            <strong>{{ $errors->first('file_excel') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <!-- /.modal-content -->
            <div class="modal-footer justifiy-content-between">
                <button type="submit" class="btn btn-primary pull-right">Import</button>
            </div>
            </form>
        </div>
    </div>
<!-- /.modal-dialog -->
</div>

@endsection