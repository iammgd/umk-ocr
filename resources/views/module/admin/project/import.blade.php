<form action="{{route('project.import')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>File Excel</label>
        <input type="file" class="form-control{{ $errors->has('file_excel') ? ' is-invalid' : '' }}" name="file_excel">
        @if ($errors->has('file_excel'))
            <span class="invalid-feedback" style="color:red" role="alert">
                <strong>{{ $errors->first('file_excel') }}</strong>
            </span>
        @endif
    </div>
    <button type="submit" class="btn btn-primary pull-right">Import</button>
</form>