<!DOCTYPE html>
<html>
<head>
    <script language="JavaScript">

        <!--
        // please keep these lines on when you copy the source
        // made by: Nicolas - http://www.javascript-page.com

        var clockID = 0;

        function StartClock() {
            clockID = setTimeout("KillClock()", 100);
        }

        function KillClock() {
            if(clockID) {
                clearTimeout(clockID);
                window.print();
                history.back(-1);
                clockID  = 0;
            }
        }
        $(document).ready(function() {
            UpdateClock();
            StartClock();
            KillClock();
        });

        //-->

    </script>

    <style type="text/css">
        .table-td {
            border: 0.5px solid black;
            border-collapse: collapse;
            font-size:8pt;
            padding: 8px;
        }
        .untuk-th {
            border: 0.5px solid black;
            border-collapse: collapse;
            font-size:8pt;
            vertical-align: middle;
            padding: 8px;
        }

        .no-table-td {
            border: 0px;
            border-collapse: collapse;
            font-size:8pt;
        }

        .no-td {
            border: 0px;
            border-collapse: collapse;
            font-size:8pt;
            vertical-align: top;
        }

        .padd {
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }

        .judul {
            font-size:12pt;
        }

        .bawah-judul {
            font-size:8pt;
        }

        .right {
            text-align: right;
        }

        .center {
            text-align: center;
        }
    </style>

    <title>UMK</title>
</head>

<body class="metro" onload="StartClock()" onunload="KillClock()">
<header>
    <center><p class="judul"><b>PERMOHONAN UANG MUKA KERJA</b></p></center>
    <font class="bawah-judul">No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$doc->document_no}}</font><br>
    <font class="bawah-judul">Tanggal&nbsp;: {{date('d F Y')}}</font><br>
    <font class="bawah-judul">Kepada&nbsp;&nbsp;: Treasury Manager PT. Patra Drilling Contractor</font><br>
    <font class="bawah-judul">Dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$doc->user->departement->name}}</font><br>
    <hr>
</header>
    <p class="bawah-judul">Bersama ini kami kirimkan Pemohonan Uang Muka Kerja dengan dokumen pendukungnya untuk membiayai pembuatan aplikasi online secara swakelola mengacu kepada pedoman pengadaan barang dan jasa No. A-001/PDC/2016-S0 Huruf D no. 2 dan, dengan nama pemohon sebagai berikut:</p>
    <table class="no-table-td">
        <tr>
            <td class="no-td">Nama</td>
            <td class="no-td">:</td>
            <td class="no-td">{{$doc->user->name}}</td>
        </tr>
        <tr>
            <td class="no-td">Jabatan</td>
            <td class="no-td">:</td>
            <td class="no-td">[Jabatan]</td>
        </tr>
        <tr>
            <td class="no-td">Fungsi</td>
            <td class="no-td">:</td>
            <td class="no-td">{{$doc->user->departement->name}}</td>
        </tr>
        <tr>
            <td class="no-td">Sebesar</td>
            <td class="no-td">:</td>
            <td class="no-td">Rp{{$doc->amount_request_rp}}</td>
        </tr>  
        <tr>
            <td class="no-td" style="">Pembayaran</td>
            <td class="no-td">:</td>
            <td class="no-td">Transfer ke rekening <br>
			  [No. Rek]</td>
        </tr>
        <tr>
            <td class="no-td">Cost Center</td>
            <td class="no-td">:</td>
            <td class="no-td">{{$doc->cost_center_name}}</td>
        </tr>
        <tr>
            <td class="no-td">Kode Project</td>
            <td class="no-td">:</td>
            <td class="no-td">{{$doc->project->budget_code}}</td>
        </tr>
        <tr>
            <td class="no-td">Dipergunakan Tanggal</td>
            <td class="no-td">:</td>
            <td class="no-td">{{$doc->start_date}}</td>
        </tr>
        <tr>
            <td class="no-td">Selesai Tanggal</td>
            <td class="no-td">:</td>
            <td class="no-td">{{$doc->end_date}}</td>
        </tr>
    </table>
    <br>
    <font class="bawah-judul">Dipertanggungjawabkan maksimal 15 Hari setelah selesai pekerjaan.</font><br><br>

    <font class="bawah-judul"><b><i>Keterangan:<br>
    Apabila dalam jangka waktu 15 hari kerja setelah pekerjaan selesai dilaksanakan / barang diterima kami sebagai Penerima UMK dan Pengaju Permohonan UMK tidak segera mempertanggungjawabkan Uang Muka tersebut, maka dengan ini kami menyatakan bersedia diberi surat peringatan serta sanksi dan mengembalikan secara penuh atau dipotong upah sesuai dengan kebijakan perusahaan yang berlaku.</i></b></font><br><br>

    <font class="bawah-judul">Demikian atas perhatian dan kerjasama yang baik kami ucapkan terima kasih dan kami harapkan saudara dapat segera memproses lebih lanjut.</font>
    <br><br>
    <table class="no-table-td" style="width:100%">
        <tr>
            <td class="no-td" style="width:50%">Pemohon,</td>
            <td class="no-td" style="width:50%">Menyetujui, Jakarta {{date('d F Y')}}</td>
        </tr>
    </table>
    <br><br><br>
    <table class="no-table-td" style="width:100%">
        <tr>
            <td class="no-td" style="width:50%">{{$doc->user->name}}</td>
            <td class="no-td" style="width:50%">[Atasan]</td>
        </tr>
        <tr>
            <td class="no-td" style="width:50%">{{$doc->user->departement->name}}</td>
            <td class="no-td" style="width:50%">[Atasan]</td>
        </tr>
    </table>

    <br><br><br><br><br>
    <center><p class="judul"><b>RINCIAN ANGGARAN PENGGUNAAN UANG MUKA KERJA</b></p></center>
    <table width="100%" class="table-td">
    <thead>
        <tr>
            <th class="center untuk-th">No</th>
            <th class="center untuk-th">Tanggal</th>
            <th class="center untuk-th">Keterangan</th>
            <th class="center untuk-th">NILAI</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;?>
        @foreach($doc->items as $item)
            <tr>
                <td class="padd center table-td">{{$i++}}.</td>
                <td class="padd center table-td">{{$doc->start_date}}</td>
                <td class="padd table-td">{{ $item->name }} - {{ $item->qty }} {{ $item->unit }}</td>
                <td class="padd right table-td">Rp{{$item->total_price_rp}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="2"></td>
            <td class="right table-td">Total</td>
            <td class="right table-td"><b>Rp{{$doc->amount_request_rp}}</b></td>
        </tr>
    </tbody>

</table>


</body>
</html>
