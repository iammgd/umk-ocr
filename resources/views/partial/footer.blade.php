<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0-beta
    </div>
    <strong>Copyright &copy; 2020 <a href="https://www.pertamina-pdc.com">Pertamina PDC</a>.</strong> All rights
    reserved.
  </footer>