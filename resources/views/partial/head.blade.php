<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('title')</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('template/dist/css/adminlte.min.css')}}">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<!-- favicon -->
<link rel="icon" href="{{ asset('images/favicon.png') }}"/>

<link rel="stylesheet" href="{{asset('js/plugins/sweetalert2/sweetalert2.min.css')}}">

<link rel="stylesheet" href="{{asset('template/plugins/select2/select2.min.css')}}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{asset('template/plugins/datepicker/datepicker3.css')}}">

<style type="text/css">
	.pagination > li > a,
	.pagination > li > span {
		color: #e12525; // use your own color here
		background-color: #e12525;
	}

	.active > li > a {
		background-color: #e12525 !important;
	}

	.pagination > .active > a,
	.pagination > .active > a:focus,
	.pagination > .active > a:hover,
	.pagination > .active > span,
	.pagination > .active > span:focus,
	.pagination > .active > span:hover {
		background-color: #e12525;
		border-color: #e12525;
}
</style>

<!-- jQuery -->
<script src="{{asset('template/plugins/jquery/jquery.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('css/custom.css')}}">