<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
      <center><span style="color:white;" class="brand-text">UMK</span></center>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
            </div>
        <div class="info">
            <a href="{{route('dashboard.index')}}" style="color:white;" class="d-block">{{Auth::user()->name}}</a>
        </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{route('dashboard.index')}}" class="nav-link" style="color:white;{{ (request()->is('/')) ? 'background-color: #008577;' : '' }}">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @if(Auth::user()->level_akses==2)
                    <li class="nav-item">
                        <a href="{{route('user.document.index')}}" class="nav-link" style="color:white;{{ (request()->is('document*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-file"></i>
                            <p>Dokumen Saya</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user.persetujuan.index')}}" class="nav-link" style="color:white;{{ (request()->is('persetujuan*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-file"></i>
                            <p>Persetujuan</p>
                        </a>
                    </li>
                @elseif(Auth::user()->level_akses==1)
                    <li class="nav-item">
                        <a href="{{route('document.index')}}" class="nav-link" style="color:white;{{ (request()->is('admin/document*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-file"></i>
                            <p>Daftar Dokumen</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('karyawan.index')}}" class="nav-link" style="color:white;{{ (request()->is('admin/karyawan*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-user"></i>
                            <p>Daftar Karyawan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.index')}}" class="nav-link" style="color:white;{{ (request()->is('admin/admin*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-user"></i>
                            <p>Daftar Admin</p>
                        </a>
                    </li>
                @endif
                <!--
                <li class="nav-item">
                    <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('ubahpassword')) ? 'background-color: #008577;' : '' }}">
                        <i class="nav-icon fa fa-unlock-alt"></i>
                        <p>Ganti Password</p>
                    </a>
                </li>

                    <li class="nav-header">LAPORAN</li>
                    <li class="nav-item">
                        <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('admin/laporan/peka/rekap')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-file"></i>
                            <p>Rekapitulasi Dokumen</p>
                        </a>
                    </li>

                    <li class="nav-header">MASTER DATA</li>
                    <li class="nav-item">
                        <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('departement*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-file"></i>
                            <p>Departement</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('admin/perusahaan*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-building"></i>
                            <p>Struktur Organisasi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('admin/partisipasi*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-cogs"></i>
                            <p>Cost Center</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('admin/fungsi*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-map-pin"></i>
                            <p>Project</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('admin/karyawan*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-user"></i>
                            <p>Activity</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('departement')}}" class="nav-link" style="color:white;{{ (request()->is('admin/allpeka*')) ? 'background-color: #008577;' : '' }}">
                            <i class="nav-icon fa fa-file"></i>
                            <p>Budget</p>
                        </a>
                    </li>
                    -->
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>