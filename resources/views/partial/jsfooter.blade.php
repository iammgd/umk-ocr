
<!-- Bootstrap 4 -->
<script src="{{asset('template/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('template/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('template/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('template/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('template/dist/js/demo.js')}}"></script>

<script src="{{asset('js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- bootstrap time picker -->
<script src="{{asset('template/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

<!-- jQuery -->
<script src="{{asset('template/plugins/jquery/jquery.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('template/plugins/chart.js/Chart.min.js')}}"></script>


<!-- Select2 -->
<script src="{{asset('template/plugins/select2/select2.full.min.js')}}"></script>

<script>
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2()
	})
</script>