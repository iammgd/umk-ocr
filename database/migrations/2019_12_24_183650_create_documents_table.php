<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('document_no');
            $table->integer('cost_center_id');
            $table->integer('departement_id');
            $table->integer('project_id');
            $table->string('description');
            $table->datetime('start_date');
            $table->datetime('end_date');
            $table->float('amount_request');
            $table->string('request_note')->nullable();
            $table->string('reason_satu')->nullable();
            $table->string('reason_dua')->nullable();
            $table->integer('created_by');
            $table->integer('status'); //0 belum diajukan, 1 pengajuan, 2 ditolak, 3 diajukan kembali, 4 diterima
            $table->integer('manage_by');
            $table->integer('manger_reason');
            $table->string('code_transfer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
