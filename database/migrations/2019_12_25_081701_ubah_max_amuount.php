<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahMaxAmuount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('org_structures', function (Blueprint $table) {
            $table->float('min_amount', 15, 2)->change();
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->float('amount_request', 15, 2)->change();
            $table->integer('manage_by')->nullable()->change();
            $table->integer('manger_reason')->nullable()->change();
            $table->string('code_transfer')->nullable()->change();
        });

        Schema::table('items', function (Blueprint $table) {
            $table->float('unit_price', 15, 2)->change();
        });

        Schema::table('budget_details', function (Blueprint $table) {
            $table->float('amount', 15, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
