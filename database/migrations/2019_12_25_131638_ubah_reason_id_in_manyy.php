<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahReasonIdInManyy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('org_structures', function (Blueprint $table) {
            $table->double('min_amount', 15, 2);
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->double('amount_request', 15, 2);
        });

        Schema::table('items', function (Blueprint $table) {
            $table->double('unit_price', 15, 2);
        });

        Schema::table('budget_details', function (Blueprint $table) {
            $table->double('amount', 15, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manyy', function (Blueprint $table) {
            //
        });
    }
}
