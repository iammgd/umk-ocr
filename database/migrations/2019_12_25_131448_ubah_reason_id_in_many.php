<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahReasonIdInMany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('org_structures', function (Blueprint $table) {
            $table->dropColumn('min_amount');
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('amount_request');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('unit_price');
        });

        Schema::table('budget_details', function (Blueprint $table) {
            $table->dropColumn('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('many', function (Blueprint $table) {
            //
        });
    }
}
