<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BudgetDetail extends Model
{
    /**
    * field yang ada di table
    **/
    protected $fillable = [
        'master_id',
        'name',
        'amount'
    ];

    /**
    *nama table
    **/
    protected $table = 'budget_details';

    /**
    *relasi
    **/
    public function master()
    {
        return $this->belongsTo('App\Model\BudgetMaster', 'master_id');
    }
}
