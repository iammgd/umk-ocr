<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
    * field yang ada di table
    **/
    protected $fillable = [
        'cost_center_id',
        'name',
        'start_date',
        'end_date',
        'kode_project'
    ];

    /**
    * nama table
    **/
    protected $table = 'projects';

    protected $appends = [
        'budget_code'
    ];

    /**
    * relasi
    **/
    public function costCenter()
    {
        return $this->belongsTo('App\Model\CostCenter', 'cost_center_id');
    }

    public function budget()
    {
        return $this->hasOne('App\Model\BudgetMaster', 'project_id')->orderBy('id', 'DESC')->take(1);
    }

    public function getBudgetCodeAttribute()
    {
        return $this->budget['code'];
    }
}
