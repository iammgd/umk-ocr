<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    /**
     * field pada tabel
     */
    protected $fillable = [
        'document_id',
        'pengaju_id',
        'approver_id',
        'status_approve'
    ];

    
    protected $table = 'approval';
}
