<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CostCenter extends Model
{
    /**
    	field yang ada di table
    **/
    protected $fillable = [
        'departement_id',
        'name',
        'profit_center'
    ];

    /**
    	nama table
    **/
    protected $table = 'cost_centers';

    /**
        relasi
    **/
    public function departement()
    {
        return $this->belongsTo('App\Model\Departement', 'departement_id');
    }

    public function projects()
    {
        return $this->hasMany('App\Model\Project', 'cost_center_id');
    }

    /**
        scope cost center sesuai dengan departement dari user terlogin
    **/
    public function scopeInMyDepartement($query)
    {
        return $query->where('departement_id', Auth::user()->departement_id);
    }
}
