<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BudgetMaster extends Model
{
    /**
    	field yang ada di table
    **/
    protected $fillable = [
        'code',
      	'year',
      	'version',
        'project_id',
        'total'
    ];

    /**
    	nama table
    **/
    protected $table = 'budget_masters';

    /**
        relasi
    **/
    public function project()
    {
        return $this->belongsTo('App\Model\Project', 'project_id');
    }

    public function details()
    {
        return $this->hasMany('App\Model\BudgetDetail', 'master_id');
    }
}
