<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Settlement extends Model
{
    /**
     * field database
     */
    protected $fillable = [
        'document_id',
        'amount',
        'file',
        'nama_file',
        'keterangan'
    ];

    /**
     * append table
     */
    protected $appends = [
        'amount_rp'
    ];

    /**
     * nama table
     */
    protected $table = 'settlement';

    /**
     * relasi ke tabel dokument
     */
    public function doc()
    {
        return $this->belongsTo('App\Model\Document', 'document_id');
    }

    /**
     * ubah amount ke bentuk rupiah
     */
    public function getAmountRpAttribute()
    {
        return 'Rp'.number_format($this->amount, 2);
    }


}
