<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
    *	field yang ada di table
    **/
    protected $fillable = [
        'document_id',
        'name',
        'quantity',
        'unit',
        'unit_price',
        'notes',
        'file',
        'total_price'
    ];

    /**
    *	nama table
    **/
    protected $table = 'items';

    protected $appends = [
        'unit_price_rp',
        'total_price_rp'
    ];

    /**
     *   relasi
    **/
    public function document()
    {
        return $this->belongsTo('App\Model\Document', 'document_id');
    }

    public function getUnitPriceRpAttribute()
    {
        return number_format($this->unit_price, 2, ',', '.');
    }
    
    public function getTotalPriceRpAttribute()
    {
        return number_format($this->total_price, 2, ',', '.');
    }
}
