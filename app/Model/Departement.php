<?php

namespace App\Model;

use App\Model\Document;
use App\Model\BudgetMaster;
use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    /**
    *field yang ada di table
    **/
    protected $fillable = [
        'id',
        'name'
    ];

    protected $appends = [
    ];

    /**
    *nama table
    **/
    protected $table = 'departements';

    public function costCenter()
    {
        return $this->hasMany('App\Model\CostCenter', 'departement_id');
    }

    public function budgetingTerpakai()
    {

    }

}
