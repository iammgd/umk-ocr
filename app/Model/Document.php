<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Services\Currency;
use Carbon\Carbon;

class Document extends Model
{
    protected $fillable = [
        'document_no',
        'cost_center_id',
        'departement_id',
        'project_id',
        'description',
        'start_date',
        'end_date',
        'amount_request',
        'request_note',
        'reason_satu',
        'reason_dua',
        'created_by',
        'status', //0 belum diajukan, 1 diajukan, 2 ditolak, 3 diterima, 4 settled
        'manage_by',
        'manger_reason',
        'code_transfer',
        'activity_id',
        'diajukan_at',
        'reject_notes',
        'no'
    ];

    protected $appends = [
        'project_name', 
        'status_text',
        'amount_request_rp', 
        'activity_name',
        'cost_center_name',
        'amount_terpakai',
        'amount_terpakai_rp',
        'amount_sisa',
        'amount_settlement_rp',
        'amount_difference_rp',
        'amount_difference',
        'umur_settle',
        'status_color'
    ];

    protected $table = 'documents';

    protected $with = ['items'];


    public function departement()
    {
        return $this->belongsTo('App\Model\Departement', 'departement_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function activity()
    {
        return $this->belongsTo('App\Model\Activity', 'activity_id');
    }

    public function costCenter()
    {
        return $this->belongsTo('App\Model\CostCenter', 'cost_center_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Model\Project', 'project_id');
    }

    public function items()
    {
        return $this->hasMany('App\Model\Item', 'document_id');
    }

    public function settlements()
    {
        return $this->hasMany('App\Model\Settlement', 'document_id');
    }

    public function getAmountSettlementRpAttribute()
    {
        return 'Rp'.number_format($this->settlements->sum('amount'), 2);
    }

    public function getAmountDifferenceRpAttribute()
    {
        return 'Rp'.number_format($this->amount_request-$this->settlements->sum('amount'), 2);
    }

    public function getAmountDifferenceAttribute()
    {
        return $this->amount_request-$this->settlements->sum('amount');
    }

    public function getUmurSettleAttribute()
    {
        if($this->status==4){
            $updated_at = Carbon::parse($this->updated_at);
            $end_date = Carbon::parse($this->end_date);
            $diff = $updated_at->diffInDays($end_date);
            if($diff>0){
                return $diff;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public function approval()
    {
        return $this->hasMany('App\Model\Approval', 'document_id');
    }

    /**
    *  scope document buatan user terlogin
    **/
    public function scopeMyDocuments($query)
    {
        return $query->where('created_by', Auth::user()->id);
    }

    /**
     *   scope document yang mengarah ke user terlogin
    **/
    public function scopeNeedApproval($query)
    {
        /*return $query->where('departement_id', Auth::user()->departement_id)->where('status', 1)->orWhere('status', 3)
        ->where('amount_request', '>=', Auth::user()->structure->min_amount);*/

        return $query->whereHas('approval', function($query_a){
            $query_a->where('approver_id', Auth::user()->id);
        });
    }

    /**
     *  field-field appends
    **/
    public function getProjectNameAttribute()
    {
        return $this->project->name;
    }

    public function getActivityNameAttribute()
    {
        return $this->activity->name;
    }

    public function getCostCenterNameAttribute()
    {
        return $this->costCenter->name;
    }

    public function getStartDateAttribute()
    {
        return date('d F Y', strtotime($this->attributes['start_date']));
    }

    public function getEndDateAttribute()
    {
        return date('d F Y', strtotime($this->attributes['end_date']));
    }

    public function getAmountRequestRpAttribute()
    {
        return number_format($this->amount_request, 2, ',', '.');
    }

    public function getAmountTerpakaiRpAttribute()
    {
        return number_format($this->items->sum('total_price'), 2, ',', '.');
    }

    public function getAmountTerpakaiAttribute()
    {
        return $this->items->sum('total_price');
    }

    public function getAmountSisaAttribute()
    {
        return $this->amount_request - $this->items->sum('total_price');
    }
   
    public function getStatusTextAttribute()
    {
        //0 belum diajukan, 1 pengajuan, 2 ditolak, 3 diterima, 4 settled
        switch ($this->status) {
            case 0:
                return "Belum Diajukan";
                break;

            case 1:
                return "Sudah Diajukan";
                break;

            case 2:
                return "Ditolak";
                break;

            case 3:
                return "Diterima";
                break;

            case 4:
                return "Settled";
                break;
            
            default:
                # code...
                break;
        }
    }  
    
    public function getStatusColorAttribute()
    {
        if($this->status_text=="Belum Diajukan"){
            return "<font style='color:#bdc3c7'>".$this->status_text."</font>";
        } else if($this->status_text=="Sudah Diajukan"){
            return "<font style='color:#2980b9'>".$this->status_text."</font>";
        } else if($this->status_text=="Ditolak"){
            return "<font style='color:#c0392b'>".$this->status_text."</font>";
        } else if($this->status_text=="Diterima"){
            return "<font style='color:#27ae60'>".$this->status_text."</font>";
        } else if($this->status_text=="Settled"){
            return "<font style='color:#f39c12'>".$this->status_text."</font>";
        }
    }
}
