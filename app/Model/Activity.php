<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    /**
    	field yang ada di table
    **/
    protected $fillable = [
        'code',
        'name'
    ];

    /**
    	nama table
    **/
    protected $table = 'activities';
}
