<?php

namespace App\Services;
use Illuminate\Http\UploadedFile;

class SettlementFileManager
{
	public function store($name, UploadedFile $file)
    {
        $file_name = $this->format($name, $file->extension());
        $destination_path = 'uploads/settlefile';
		$file->move($destination_path, $file_name);
        
        return $destination_path.'/'.$file_name;
    }

    protected function format($name, $extension)
    {
        return preg_replace('/\s+/', '_', $name) . '.' . $extension;
    }

    public function delete($filename)
    {
        @unlink($filename);
    }
}