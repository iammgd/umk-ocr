<?php

namespace App\Services;
use App\Model\Document;
use App\Model\Item;
use Auth;
use Carbon\Carbon;

class ItemServices
{
    /**
     * The PromoPhotoManager instance.
     *
     * @var ItemFileManager
     */
    protected $file;

    /**
     * Create a new PromoInsertor instance.
     *
     * @param  PromoPhotoManager  $photos 
     * @return void 
     */
    public function __construct(ItemFileManager $file)
    {
        $this->file = $file;
    }


    /**
     * Perform insertion of new promo.
     * 
     * @param  array   $data
     * @return void
     */
    public function insert(array $data)
    {
        if(isset($data['file'])) $data['file'] = $this->storeFile($data);


        $data['document_id'] = $data['document_id'];  
        $data['name'] = $data['name'];
        $data['quantity'] = $data['quantity'];
        $data['unit'] = $data['unit'];
        $data['unit_price'] = str_replace(',', '', $data['unit_price']);
        $data['total_price'] = $this->totalPrice($data['quantity'], str_replace(',', '', $data['unit_price']));
        $data['notes'] = $data['notes'];
        if(isset($data['file'])) $data['file'] = $data['file'];

        $doc = Item::create($data);
    }

    /**
     * Store file item to file storage.
     *
     * @param  array  $data
     * @return string
     */
    protected function storeFile(array $data)
    {
        return $this->file->store(
            str_random(16), 
            $data['name'], 
            $data['file']
        );
    }

    protected function totalPrice($qty, $harga_qty)
    {
        return $qty*$harga_qty;
    }

    public function delete($item_id)
    {
        $item = Item::destroy($item_id);
    }

}