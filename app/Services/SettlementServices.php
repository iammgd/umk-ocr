<?php

namespace App\Services;
use App\Model\Settlement;
use App\Services\SettlementFileManager;
use Auth;
use Carbon\Carbon;

class SettlementServices
{
    /**
     * The SettlementFileManager instance.
     *
     * @var SettlementFileManager
     */
    protected $file;

    /**
     * Create a new SettlementInsertor instance.
     *
     * @param  SettlementFileManager  $photos 
     * @return void 
     */
    public function __construct(SettlementFileManager $file)
    {
        $this->file = $file;
    }


    /**
     * Perform insertion of new settlement.
     * 
     * @param  array   $data
     * @return void
     */
    public function insert(array $data)
    {
        $data['file'] = $this->storeFile($data);
        $data['nama_file'] = $data['nama_file'];  
        $data['document_id'] = $data['document_id'];  
        $data['amount'] = str_replace(',', '', $data['amount']);
        $data['keterangan'] = $data['keterangan'];

        $doc = Settlement::create($data);
    }

    /**
     * Store file item to file storage.
     *
     * @param  array  $data
     * @return string
     */
    protected function storeFile(array $data)
    {
        return $this->file->store(
            str_random(16),
            $data['file']
        );
    }

    public function delete($settle_id)
    {
        $settle = Settlement::destroy($settle_id);
    }

}