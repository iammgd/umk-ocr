<?php

namespace App\Services;

use App\Model\Document;
use App\Model\Settlement;
use App\Model\Item;
use App\Utilities\DocumentNoGenerator;
use App\Model\Approval;
use Auth;
use Carbon\Carbon;
use App\User;

class DocumentServices
{
    /**
     * Perform insertion of new promo.
     * 
     * @param  array   $data
     * @return void
     */
    public function insert(array $data)
    {
        $data['document_no'] = (new DocumentNoGenerator)->createDocNo();   
        $data['no'] = (new DocumentNoGenerator)->createDocNoUrut();   
        $data['created_by'] = Auth::user()->id;
        $data['status'] = 0; // belum diajukan
        $data['departement_id'] = Auth::user()->departement_id;
        $data['amount_request'] = str_replace(',', '', $data['amount_request']);

        $doc = Document::create($data);
    }

    public function ajukan($document_id)
    {
        if(Item::where('document_id', $document_id)->exists()){
            $doc = Document::find($document_id);
            $doc->status = 1; //sudah diajukan
            $doc->diajukan_at = Carbon::now();
            $doc->save(); 

            $approve = new Approval();
            $approve->document_id = $doc->id;
            $approve->pengaju_id = Auth::user()->id;
            $approve->approver_id = self::searchApprover();
            $approve->status_approve = 0;
            $approve->save();
            return true;
        } else {
            return false;
        }
    }

    public function settle($document_id)
    {
        if(Settlement::where('document_id', $document_id)->exists()){
            $doc = Document::find($document_id);
            $doc->status = 4; //settle
            $doc->save(); 
            return true;
        } else {
            return false;
        }
    }

    public static function searchApprover()
    {
        $approver = User::where('departement_id', Auth::user()->departement_id)->where('urutan', Auth::user()->urutan-1)->first();
        return $approver->id;
    }

    public function reject(array $data, $document_id)
    {   
        $doc = Document::find($document_id);
        $doc->status = 2; //ditolak
        $doc->reject_notes = $data['reject_notes'];
        $doc->manage_by = Auth::user()->id;
        //$doc->manage_reason = $data['manage_reason'];
        $doc->save(); 
    }

    public function approve($document_id)
    {   
        $doc = Document::find($document_id);
        $doc->status = 3; //diterima
        $doc->manage_by = Auth::user()->id;
        $doc->manage_reason = NULL;
        $doc->save(); 

        $appr = Approval::where('document_id', $document_id)->first();
        $approve = Approval::find($appr->id);
        $approve->status_approve = 1;
        $approve->save();
    }
    
    public function close(array $data, $document_id)
    {   
        $doc = Document::find($document_id);
        $doc->status = 4; //di-close
        $doc->code_transfer = $data['code_transfer']; //di-close
        $doc->save();
    }

    public function delete(Document $doc)
    {
        $doc->delete();
    }

}