<?php

namespace App\Services;
use App\User;

class AdminServices
{
    /**
     * Perform insertion of new promo.
     * 
     * @param  array   $data
     * @return void
     */
    public function insert(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        $data['level_akses'] = 1;
        $data['departement_id'] = 0;
        $data['urutan'] = -1;

        if($this->emailIsExists($data['email'])){
            return false;
        } else {
            User::create($data);
            return true;
        }
    }

    public function emailIsExists($email)
    {
        if(User::where('email', $email)->exists()){
            return true;
        } else {
            return false;
        }
    }
}