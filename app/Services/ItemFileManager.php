<?php

namespace App\Services;
use Illuminate\Http\UploadedFile;

class ItemFileManager
{
	public function store($id, $name, UploadedFile $file)
    {
        $file_name = $this->format($id, $name, $file->extension());
        $destination_path = 'uploads/itemfile';
		$file->move($destination_path, $file_name);
        
        return $destination_path.'/'.$file_name;
    }

    protected function format($id, $name, $extension)
    {
        return $id . '_' . preg_replace('/\s+/', '_', $name) . '.' . $extension;
    }

    public function delete($filename)
    {
        @unlink($filename);
    }
}