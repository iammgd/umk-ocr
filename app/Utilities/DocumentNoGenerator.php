<?php

namespace App\Utilities;
use App\Model\Document;

class DocumentNoGenerator
{
	public function createDocNo()
	{
		$no = sprintf('%07d', $this->createDocNoUrut());
		return 'DCUMK-'.$no;
	}

	public function createDocNoUrut()
	{
		$doc = Document::latest()->get();
		if($doc->isEmpty()){
			return 1;
		} else {
			$doc = $doc->first();
			return $doc->no+1;
		}
	}
}