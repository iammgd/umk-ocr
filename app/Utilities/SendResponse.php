<?php

namespace App\Utilities;

class SendResponse
{
    public static function success($data, $code){
        //$response['response_code'] = $code;
        $response['Error'] = false;
        $response['Message'] = 'success';
        $response['Data'] = $data;
        
        return response()->json($response, $code);
    }

    public static function successWithMessage($message, $code){
        //$response['response_code'] = $code;
        $response['Error'] = false;
        $response['Message'] = $message;
        
        return response()->json($response, $code);
    }

    public static function delete(){
        $response['Error'] = false;
        $response['Message'] = 'success';
        
        return response()->json($response, $code);
    }

    public static function fail($message, $code){
        //$response['response_code'] = $code;
        $response['Error'] = true;
        $response['Message'] = $message;
        
        return response()->json($response, $code);
    }

    public static function failForLogin($message, $code){
        $response['Data'] = null;
        $response['Error'] = true;
        $response['Message'] = $message;
        
        return response()->json($response, $code);
    }
}
