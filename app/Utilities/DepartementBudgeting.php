<?php

namespace App\Utilities;

use App\Model\Document;
use App\Model\Departement;
use App\Model\Settlement;
use App\Model\BudgetMaster;
use Auth;
use App\User;

class DepartementBudgeting
{
    public static function getBudget()
    {
        $dept_id = Auth::user()->departement_id;
        $dept = Departement::find($dept_id);

        $diterima = Document::where('departement_id', $dept_id)->where('status', 3)->sum('amount_request');
        $settled = Settlement::whereHas('doc', function($query) use($dept_id){
                $query->where('departement_id', $dept_id);
            })->sum('amount');

        $dept['total_terpakai'] = $settled + $diterima;

        $dept['total_budget'] = BudgetMaster::whereHas('project', function($query) use($dept_id){
            $query->whereHas('costCenter', function($query_c) use($dept_id){
                $query_c->where('departement_id', $dept_id);
            });
        })->sum('total');

        $persen = ($dept['total_terpakai']/$dept['total_budget']) * 100;

        $dept['persen_terpakai'] = (round($persen,2));
        $dept['persen_sisa'] = (100 - $dept['persen_terpakai']);

        $dept['total_terpakai'] = number_format($dept['total_terpakai'],2);
        $dept['total_budget'] = number_format($dept['total_budget'],2);

        return $dept;
    }
}