<?php

namespace App\Imports;

use Auth;
use App\Model\Project;
use App\Model\Departement;
use App\Model\CostCenter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class ProjectImport implements ToModel, WithBatchInserts
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        //dept
        // if(!Departement::where('name', $row[0])->exists()){
        //     $dept = Departement::create([
        //         'name' => $row[0]
        //     ]);
        // }


        //cost_center
        // if(!CostCenter::where('name', $row[1])->exists()){
        //     $cost = new CostCenter();
        //     $cost->departement_id = $this->dept($row[3]);
        //     $cost->name = $row[1];
        //     $cost->profit_center = $row[2];
        //     $cost->save();
        // }

        //project
        if(!Project::where('name', $row[1])->exists()){
            $cost = new Project();
            $cost->cost_center_id = $this->costCenter($row[3]);
            $cost->kode_project = $row[1];
            $cost->name = $row[2];
            $cost->save();
        }
    }

    public function dept($dept_name)
    {
        $dept = Departement::where('name', $dept_name)->first();
        return $dept->id;
    }

    public function costCenter($cost_name)
    {
        $cost = CostCenter::where('name', $cost_name)->first();
        return $cost->id;
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
