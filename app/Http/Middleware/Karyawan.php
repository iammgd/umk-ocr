<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Karyawan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::User()->level_akses=='2'){
              return $next($request);
            } else {
              return response(view('errors.accessdenied'));
            }
        }
    }
}
