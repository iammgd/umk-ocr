<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cost_center_id' => 'required|integer',
            'activity_id' => 'required|integer',
            'project_id' => 'nullable|integer',
            'description' => 'required|String',
            'start_date' => 'required|String',
            'end_date' => 'required|String',
            'amount_request' => 'required|String',
            'request_note' => 'nullable|String',
            'reason_satu' => 'nullable|String',
            'reason_dua' => 'nullable|String',
        ];
    }

    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'required' => ':attribute harus diisi.',
        ];
    }
}
