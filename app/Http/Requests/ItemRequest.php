<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_id' => 'required|integer',
            'name' => 'required|String',
            'quantity' => 'required|integer',
            'unit' => 'required|String',
            'unit_price' => 'required|String',
            'notes' => 'nullable|String',
            'file' => 'nullable|file|mimes:jpeg,bmp,png,gif,svg,pdf',
        ];
    }

    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'required' => ':attribute harus diisi.',
        ];
    }
}
