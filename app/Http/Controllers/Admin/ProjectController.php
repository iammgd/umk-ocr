<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProjectImport;

class ProjectController extends Controller
{
    public function importHtml()
    {
        return view('module.admin.project.import');
    }

    public function import(Request $req)
    {
        Excel::import(new ProjectImport, $req->file('file_excel'));

        echo "successed";
    }
}
