<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\AdminUpdateRequest;
use App\Services\AdminServices;
use App\Utilities\FlashMessage;
use App\User;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        $admins = User::where('level_akses', '1')->paginate(20);
        return view('module.admin.admin.index', compact('admins'));
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('module.admin.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request, AdminServices $service)
    {
        if($service->insert($request->all())){
        	return redirect()->route('admin.index')->with('message', 
            new FlashMessage('Admin telah berhasil ditambahkan!', 
                FlashMessage::SUCCESS));
        } else {
        	return redirect()->route('admin.index')->with('message', 
            new FlashMessage('Gagal menambahkan admin dikarenakan email sudah didaftarkan.', 
                FlashMessage::DANGER));
        }
    }

        /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(User $admin)
    {
        return view('module.admin.admin.detail', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(User $admin)
    {
        return view('module.admin.admin.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateRequest $request, User $admin)
    {
        $admin->fill(collect($request->toArray())->filter()->toArray());
        if($request->password!='') $admin->password = bcrypt($admin->password);
        $admin->save();
        return redirect()->route('admin.detail', [$admin])->with('message', 
            new FlashMessage('Admin telah berhasil diubah!', 
                FlashMessage::SUCCESS));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $admin)
    {
        $admin->delete();
        return redirect()->route('admin.index')->with('message', 
            new FlashMessage('Admin telah berhasil dihapus!', 
                FlashMessage::WARNING));
    }
}
