<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Document;
use App\Model\CostCenter;
use App\Services\DocumentServices;
use App\Utilities\FlashMessage;
use PDF;
use Auth;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function index()
    {
        $cost_centers = CostCenter::latest()->get();
        $docs = Document::latest()->paginate(10);
        return view('module.admin.document.index', compact('docs', 'cost_centers'));
    }

    public function search(Request $req)
    {
        $doc_no = $req->doc_no;
        $status = $req->status;
        $cost_center_id = $req->cost_center_id;
        $start_date_from = $req->start_date_from;
        $start_date_to = $req->start_date_to;

        $cost_centers = CostCenter::latest()->get();
        $docs = Document::latest();

        if($cost_center_id!="all"){
            $docs = $docs->where('cost_center_id', $cost_center_id);
        }

        if($status!="all"){
            $docs = $docs->where('status', $status);
        }

        if($doc_no!=''){
            $docs = $docs->where('document_no', 'LIKE', '%' . $doc_no . '%');
        }

        if($start_date_from!='' && $start_date_to!=''){
            $docs = $docs->where('start_date', '>=', $start_date_from.' 00:00:00')->where('start_date', '<=', $start_date_to.' 23:59:59');
        }

        $docs = $docs->paginate(10);
        return view('module.admin.document.index', compact('docs', 'cost_centers', 'doc_no', 'status', 'cost_center_id', 'start_date_from', 'start_date_to'));

    }

    public function docPrint($document_id)
    {
        $doc = Document::find($document_id);
        $pdf = PDF::loadView('module.admin.document.print', ['user' => Auth::user(), 'doc' => $doc]);
        return $pdf->stream('Cetak-UMK.pdf');
    }

    public function show(Document $doc)
    {
        return view('module.admin.document.detail', compact('doc'));
    }

    public function delete(Document $doc, DocumentServices $service)
    {
        $service->delete($doc);
        return redirect()->route('document.index')->with('message', 
            new FlashMessage('Document telah berhasil dihapus!', 
                FlashMessage::WARNING));

    }
}
