<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\KaryawanRequest;
use App\Http\Requests\KaryawanUpdateRequest;
use App\Services\KaryawanServices;
use App\Utilities\FlashMessage;
use App\User;
use App\Model\Departement;
use App\Http\Controllers\Controller;

class KaryawanController extends Controller
{
    public function index()
    {
        $karyawans = User::where('level_akses', '2')->paginate(20);
        return view('module.admin.karyawan.index', compact('karyawans'));
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departements = Departement::latest()->get();
        return view('module.admin.karyawan.create', compact('departements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KaryawanRequest $request, KaryawanServices $service)
    {
        if($service->insert($request->all())){
        	return redirect()->route('karyawan.index')->with('message', 
            new FlashMessage('Karyawan telah berhasil ditambahkan!', 
                FlashMessage::SUCCESS));
        } else {
        	return redirect()->route('karyawan.index')->with('message', 
            new FlashMessage('Gagal menambahkan karyawan dikarenakan email sudah didaftarkan.', 
                FlashMessage::DANGER));
        }
    }

        /**
     * Display the specified resource.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function show(User $karyawan)
    {
        return view('module.admin.karyawan.detail', compact('karyawan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function edit(User $karyawan)
    {
        $departements = Departement::latest()->get();
        return view('module.admin.karyawan.edit', compact('karyawan', 'departements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function update(KaryawanUpdateRequest $request, User $karyawan)
    {
        $karyawan->fill(collect($request->toArray())->filter()->toArray());
        if($request->password!='') $karyawan->password = bcrypt($karyawan->password);
        $karyawan->save();
        return redirect()->route('karyawan.detail', [$karyawan])->with('message', 
            new FlashMessage('Karyawan telah berhasil diubah!', 
                FlashMessage::SUCCESS));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $karyawan)
    {
        $karyawan->delete();
        return redirect()->route('karyawan.index')->with('message', 
            new FlashMessage('Karyawan telah berhasil dihapus!', 
                FlashMessage::WARNING));
    }
}
