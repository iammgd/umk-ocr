<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Services\SettlementServices;
use App\Utilities\SendResponse;
use App\Http\Controllers\Controller;
use App\Model\Settlement;
use App\Model\Document;

class SettlementController extends Controller
{
    public function store(Request $request, SettlementServices $service)
    {
        $service->insert($request->all());
        return SendResponse::successWithMessage("Berhasil meng-upload settlement.", 200);
    }

    public function index(Request $request)
    {
        $document = Document::find($request->document_id);
        $settle = $document->settlements()->get();

        $response['Error'] = false;
        $response['Message'] = 'success';
        $response['amount_doc'] = $document->amount_request_rp;
        $response['amount_settle'] = $document->amount_settlement_rp;
        $response['difference'] = $document->amount_difference_rp;
        $response['Data'] = $settle;
        
        return response()->json($response, 200);
    }

    public function delete(Request $request, SettlementServices $service)
    {
        $service->delete($request->id);
        return SendResponse::successWithMessage("Berhasil menghapus settlement.", 200);
    }
}
