<?php

namespace App\Http\Controllers\API;

use App\Model\Activity;
use Illuminate\Http\Request;
use App\Utilities\SendResponse;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    
    public function index()
    {
        $activities = Activity::latest()->get();
        return SendResponse::success($activities, 200);
    }

}
