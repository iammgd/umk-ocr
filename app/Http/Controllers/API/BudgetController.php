<?php

namespace App\Http\Controllers\API;

use App\Model\BudgetMaster;
use App\Model\Project;
use Illuminate\Http\Request;
use App\Utilities\SendResponse;
use App\Http\Controllers\Controller;

class BudgetController extends Controller
{
    
    public function index($project_id)
    {
    	try {
            $project = Project::find($project_id);
        	return SendResponse::success($project->budget, 200);
        } catch (\Exception $e) {
        	return SendResponse::fail('Gagal, karena: '.$e->getMessage(), 500);
        }
    }

}
