<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Hash;
use App\Utilities\SendResponse;

class LoginController extends Controller
{

	public function login(Request $request){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $user['token']  = $user->createToken('MyApp')->accessToken;
            return SendResponse::success($user, 200);
        } 
        else{
	        $message	= 'Username atau password yang anda masukan salah!';
            return SendResponse::failForLogin($message, 200);
        } 
    }

}