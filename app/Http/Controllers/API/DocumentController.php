<?php

namespace App\Http\Controllers\API;

use App\Model\Document;
use Illuminate\Http\Request;
use App\Utilities\SendResponse;
use App\Services\DocumentServices;
use App\Http\Controllers\Controller;
use Auth;

class DocumentController extends Controller
{
    public function myDocuments()
    {
        $doc = Document::MyDocuments()->latest()->get();
        return SendResponse::success($doc, 200);
    }

    public function detail(Request $req)
    {
        $document_id = $req->id;
        $doc = Document::with('items')->find($document_id);
        return SendResponse::success($doc, 200);
    }

    public function needApproval()
    {
        if(Auth::user()->level_akses==1){
            $doc = [];
        } else {
            $doc = Document::NeedApproval()->latest()->get();
        }
        return SendResponse::success($doc, 200);
    }

    public function store(Request $request, DocumentServices $service)
    {   
        $service->insert($request->all());
        return SendResponse::successWithMessage("Dokumen berhasil disimpan.", 200);
    }

    public function ajukan(Request $req, DocumentServices $service)
    {
        $doc = Document::find($req->document_id);
        if($doc->status == 0){
            if($doc->amount_request!=$doc->amount_terpakai){
                return SendResponse::successWithMessage("Dokumen tidak dapat diajukan, karena total amount tidak seimbang.", 200);
            } else {
                if($service->ajukan($req->document_id)){
                    return SendResponse::successWithMessage("Dokumen berhasil diajukan.", 200);
                } else {
                    return SendResponse::fail("Dokumen tidak dapat diajukan, karena belum ada item yang ditambahkan.", 200);
                }
            }
        } else {
            return SendResponse::fail("Tidak dapat mengajukan dokumen, karena dokumen sudah pernah diajukan.", 200);
        }
    }

    public function settle(Request $req, DocumentServices $service)
    {
        $doc = Document::find($req->document_id);
        if($doc->status == 3){
            if($doc->amount_request<$doc->settlements->sum('amount')){
                return SendResponse::fail("Settlement tidak dapat diajukan, karena total amount pada settlement melebihi total amount pada dokumen.", 200);
            } else {
                if($service->settle($req->document_id)){
                    return SendResponse::successWithMessage("Settlement berhasil diajukan.", 200);
                } else {
                    return SendResponse::fail("Settlement tidak dapat diajukan, karena belum ada data settlement yang ditambahkan.", 200);
                }
            }
        } else {
            return SendResponse::fail("Untuk dapat mengajukan settlement, status dokumen harus dalam keadaan diterima.", 200);
        }
    }

    public function reject(Request $req, DocumentServices $service)
    {
        $service->reject($req->all(), $req->document_id);
        return SendResponse::successWithMessage("Dokumen berhasil di-reject.", 200);
    }

    public function approve(Request $req, DocumentServices $service)
    {
        $doc = Document::find($req->document_id);
        if($doc->status == 1){
            $service->approve($req->document_id);
            return SendResponse::successWithMessage("Dokumen berhasil di-approve.", 200);
        } else {
            return SendResponse::fail("Tidak dapat menerima dokumen, karena dokumen sudah pernah diterima/ditolak.", 200);
        }
        
    }

    public function close(Request $req, DocumentServices $service)
    {
        $service->close($req->all(), $req->document_id);
        return SendResponse::successWithMessage("Dokumen berhasil di-close.", 200);
    }

}
