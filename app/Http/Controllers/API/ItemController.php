<?php

namespace App\Http\Controllers\API;

use App\Model\Item;
use Illuminate\Http\Request;
use App\Services\ItemServices;
use App\Utilities\SendResponse;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    public function store(Request $request, ItemServices $service)
    {
        $service->insert($request->all());
        return SendResponse::successWithMessage("Item berhasil ditambahkan.", 200);
    }

    public function delete(Request $request, ItemServices $service)
    {
        $item = Item::find($request->item_id);
        if($item->document->status>=3){
            return SendResponse::fail("Anda tidak diizinkan lagi untuk menghapus item.", 200);
        } else {
            $service->delete($item->id);
            return SendResponse::successWithMessage("Item berhasil dihapus.", 200);
        }
    }
}
