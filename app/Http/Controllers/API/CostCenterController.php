<?php

namespace App\Http\Controllers\API;

use App\Model\CostCenter;
use Illuminate\Http\Request;
use App\Utilities\SendResponse;
use App\Http\Controllers\Controller;

class CostCenterController extends Controller
{

    public function index()
    {
        $cost_center = CostCenter::InMyDepartement()->latest()->get();
        return SendResponse::success($cost_center, 200);
    }

    
}
