<?php

namespace App\Http\Controllers\API;

use App\Model\Departement;
use App\Model\Document;
use App\Model\Settlement;
use App\Model\BudgetMaster;
use Illuminate\Http\Request;
use App\Utilities\SendResponse;
use App\Utilities\DepartementBudgeting;
use App\Http\Controllers\Controller;
use Auth;

class DepartementController extends Controller
{

    public function index(Request $req)
    {
        $dept = DepartementBudgeting::getBudget();
        return SendResponse::success($dept, 200);
    }

    
}
