<?php

namespace App\Http\Controllers\API;

use App\Model\Project;
use Illuminate\Http\Request;
use App\Utilities\SendResponse;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        $projects = Project::where('cost_center_id', $request->cost_center_id)->latest()->get();
        return SendResponse::success($projects, 200);
    }
}
