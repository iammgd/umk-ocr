<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utilities\DepartementBudgeting;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $dept['persen_terpakai'] = 0;
        $dept['persen_sisa'] = 0;
        if(Auth::user()->level_akses==2){
            $dept = DepartementBudgeting::getBudget();
        }
        return view('module.dashboard.index', compact('dept'));
    }
}