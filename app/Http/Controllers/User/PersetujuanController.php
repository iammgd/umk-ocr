<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Model\Document;
use App\Http\Controllers\Controller;
use App\Services\DocumentServices;
use App\Http\Requests\TolakRequest;
use App\Utilities\FlashMessage;

class PersetujuanController extends Controller
{
    public function persetujuan()
    {
        $docs = Document::NeedApproval()->latest()->paginate(20);
        return view('module.user.persetujuan.index', compact("docs"));
    }

    public function show(Document $doc)
    {
        return view('module.user.persetujuan.detail', compact('doc'));
    }

    public function tolakHtml($doc_id)
    {
        return view('module.user.persetujuan.tolak', compact('doc_id'));
    }

    public function tolak(TolakRequest $req, DocumentServices $service)
    {
        $service->reject($req->all(), $req->doc_id);
        return redirect()->route('user.persetujuan.detail', [$req->doc_id])->with('message', 
            new FlashMessage('Dokumen berhasil ditolak.', 
                FlashMessage::SUCCESS));
    }

    public function terima($doc_id, DocumentServices $service)
    {
        $doc = Document::find($doc_id);
        if($doc->status == 1){
            $service->approve($doc_id);
            return redirect()->route('user.persetujuan.detail', [$doc_id])->with('message', 
                new FlashMessage('Dokumen berhasil diterima.', 
                    FlashMessage::SUCCESS));
        } else {
            return redirect()->route('user.persetujuan.detail' [$doc_id])->with('message', 
                new FlashMessage('Tidak dapat menerima dokumen, karena dokumen sudah pernah diterima/ditolak.', 
                    FlashMessage::DANGER));
        }
        
    }
}
