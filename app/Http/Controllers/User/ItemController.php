<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ItemRequest;
use App\Services\ItemServices;
use App\Model\Item;
use App\Utilities\FlashMessage;

class ItemController extends Controller
{
    public function store(ItemRequest $request, ItemServices $service)
    {
        $service->insert($request->all());
        return redirect()->route('user.document.detail', [$request->document_id])->with('message', 
            new FlashMessage('Item telah berhasil ditambahkan!', 
                FlashMessage::SUCCESS));
    }

    public function delete($item_id, ItemServices $service)
    {
        $item = Item::find($item_id);
        if($item->document->status>=3){
            return redirect()->route('user.document.detail', [$item->document_id])->with('message', 
            new FlashMessage('Item gagal dihapus, Anda tidak diizinkan lagi untuk menghapus item.', 
                FlashMessage::DANGER));
        } else {
            $service->delete($item_id);
            return redirect()->route('user.document.detail', [$item->document_id])->with('message', 
            new FlashMessage('Item berhasil dihapus.', 
                FlashMessage::SUCCESS));
        }
    }
}
