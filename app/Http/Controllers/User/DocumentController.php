<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Model\Document;
use App\Model\Activity;
use App\Model\Project;
use App\Model\CostCenter;
use App\Http\Controllers\Controller;
use App\Services\DocumentServices;
use App\Http\Requests\DocumentRequest;
use App\Utilities\FlashMessage;

class DocumentController extends Controller
{
    public function index()
    {
        $docs = Document::MyDocuments()->latest()->paginate(20);
        return view('module.user.document.index', compact("docs"));
    }

    public function search(Request $req)
    {
        $doc_no = $req->doc_no;
        $status = $req->status;
        $start_date_from = $req->start_date_from;
        $start_date_to = $req->start_date_to;

        $cost_centers = CostCenter::latest()->get();
        $docs = Document::MyDocuments()->latest();

        if($status!="all"){
            $docs = $docs->where('status', $status);
        }

        if($doc_no!=''){
            $docs = $docs->where('document_no', 'LIKE', '%' . $doc_no . '%');
        }

        if($start_date_from!='' && $start_date_to!=''){
            $docs = $docs->where('start_date', '>=', $start_date_from.' 00:00:00')->where('start_date', '<=', $start_date_to.' 23:59:59');
        }

        $docs = $docs->paginate(10);
        return view('module.user.document.index', compact('docs', 'doc_no', 'status', 'cost_center_id', 'start_date_from', 'start_date_to'));

    }

    public function show(Document $doc)
    {
        return view('module.user.document.detail', compact('doc'));
    }

    public function create()
    {
        $cost_centers = CostCenter::latest()->get();
        $activities = Activity::latest()->get();
        return view('module.user.document.create', compact('cost_centers', 'activities'));
    }

    public function store(DocumentRequest $request, DocumentServices $service)
    {   
        $service->insert($request->all());
        return redirect()->route('user.document.index')->with('message', 
            new FlashMessage('Dokumen telah berhasil ditambahkan!', 
                FlashMessage::SUCCESS));
    }

    public function getProjects($cost_center_id)
    {
        return Project::where('cost_center_id', $cost_center_id)->pluck('name', 'id');
    }

    public function ajukan($doc, DocumentServices $service)
    {
        $doc = Document::find($doc);
        if($doc->status == 0){
            if($doc->amount_request!=$doc->amount_terpakai){
                return redirect()->route('user.document.detail', [$doc->id])->with('message', 
                    new FlashMessage('Dokumen tidak dapat diajukan, karena total amount tidak seimbang.', 
                        FlashMessage::DANGER));
            } else {
                if($service->ajukan($doc->id)){
                    return redirect()->route('user.document.detail', [$doc->id])->with('message', 
                        new FlashMessage('Dokumen berhasil diajukan.', 
                            FlashMessage::SUCCESS));
                } else {
                    return redirect()->route('user.document.detail', [$doc->id])->with('message', 
                    new FlashMessage('Dokumen tidak dapat diajukan, karena belum ada item yang ditambahkan.', 
                        FlashMessage::DANGER));
                }
            }
        } else {
            return redirect()->route('user.document.detail', [$doc->id])->with('message', 
                    new FlashMessage('Tidak dapat mengajukan dokumen, karena dokumen sudah pernah diajukan.', 
                        FlashMessage::DANGER));
        }
    }

    public function settle($doc, DocumentServices $service)
    {
        $doc = Document::find($doc);
        if($doc->status == 3){
            if($doc->amount_request<$doc->settlements->sum('amount')){
                return redirect()->route('user.document.detail', [$doc->id])->with('message', 
                    new FlashMessage('Settlement tidak dapat diajukan, karena total amount pada settlement melebihi total amount pada dokumen.', 
                        FlashMessage::DANGER));
            } else {
                if($service->settle($doc->id)){
                    return redirect()->route('user.document.detail', [$doc->id])->with('message', 
                        new FlashMessage('Settlement berhasil diajukan.', 
                            FlashMessage::SUCCESS));
                } else {
                    return redirect()->route('user.document.detail', [$doc->id])->with('message', 
                    new FlashMessage('Settlement tidak dapat diajukan, karena belum ada data settlement yang ditambahkan.', 
                        FlashMessage::DANGER));
                }
            }
        } else {
            return SendResponse::fail("Untuk dapat mengajukan settlement, status dokumen harus dalam keadaan diterima.", 200);
        }
    }
}
