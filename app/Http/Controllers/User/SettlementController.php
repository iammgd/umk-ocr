<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SettlementServices;
use App\Model\Settlement;
use App\Model\Document;
use App\Utilities\FlashMessage;
use App\Http\Requests\SettlementRequest;

class SettlementController extends Controller
{
    public function store(SettlementRequest $request, SettlementServices $service)
    {
        $data = $request->all();
        $data['nama_file'] = $request->file->getClientOriginalName();
        $service->insert($data);
        return redirect()->route('user.document.detail', [$request->document_id])->with('message', 
            new FlashMessage('Berhasil menambahkan file settlement.', 
                FlashMessage::SUCCESS));
    }

    public function delete($settle_id, SettlementServices $service)
    {
        $settle = Settlement::find($settle_id);
        $service->delete($settle->id);
        return redirect()->route('user.document.detail', [$settle->document_id])->with('message', 
            new FlashMessage('Berhasil menghapus settlement.', 
                FlashMessage::SUCCESS));
        
    }
}
