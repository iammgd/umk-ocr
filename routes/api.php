<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\LoginController@login');

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('costcenter', 'API\CostCenterController@index');
	Route::post('projects', 'API\ProjectController@index');
	Route::post('activities', 'API\ActivityController@index');
	Route::post('departement', 'API\DepartementController@index');
	Route::post('budget/{project_id}', 'API\BudgetController@index');

	Route::prefix('document')->group(function () {
		Route::post('mydocuments', 'API\DocumentController@myDocuments');
		Route::post('needapproval', 'API\DocumentController@needApproval');
		Route::post('store', 'API\DocumentController@store');
		Route::post('item/store', 'API\ItemController@store');
		Route::post('item/delete', 'API\ItemController@delete');
		Route::post('detail', 'API\DocumentController@detail');
		Route::post('ajukan', 'API\DocumentController@ajukan');
		Route::post('reject', 'API\DocumentController@reject');
		Route::post('approve', 'API\DocumentController@approve');
		Route::post('close', 'API\DocumentController@close');
		Route::post('settlement/store', 'API\SettlementController@store');
		Route::post('settlement', 'API\SettlementController@index');
		Route::post('settlement/delete', 'API\SettlementController@delete');
		Route::post('settle', 'API\DocumentController@settle');
	});
});