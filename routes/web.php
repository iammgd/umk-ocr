<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('download-apk', function(){
	return response()->download(public_path("UMK.apk"));
})->name('download.apk');

Route::group(['middleware' => ['auth'] ], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard.index');

    Route::group(['middleware' => ['Admin']], function () {
        Route::prefix('admin')->group(function () {
            Route::prefix('project')->group(function () {
                Route::get('import', 'Admin\ProjectController@importHtml')->name('project.import.html');
                Route::post('import', 'Admin\ProjectController@import')->name('project.import');
            });
            Route::prefix('document')->group(function () {
                Route::get('/', 'Admin\DocumentController@index')->name('document.index');
                Route::get('/search', 'Admin\DocumentController@search')->name('document.search');
                Route::get('detail/{doc}', 'Admin\DocumentController@show')->name('document.detail');
                Route::delete('delete/{doc}', 'Admin\DocumentController@delete')->name('document.delete');
            });
            Route::prefix('karyawan')->group(function () {
                Route::get('/', 'Admin\KaryawanController@index')->name('karyawan.index');
                Route::get('/search', 'Admin\KaryawanController@search')->name('karyawan.search');
                Route::get('create', 'Admin\KaryawanController@create')->name('karyawan.create');
                Route::post('store', 'Admin\KaryawanController@store')->name('karyawan.store');
                Route::get('detail/{karyawan}', 'Admin\KaryawanController@show')->name('karyawan.detail');
                Route::get('edit/{karyawan}', 'Admin\KaryawanController@edit')->name('karyawan.edit');
                Route::put('update/{karyawan}', 'Admin\KaryawanController@update')->name('karyawan.update');
                Route::delete('delete/{karyawan}', 'Admin\KaryawanController@destroy')->name('karyawan.delete');
                Route::get('import', 'Admin\KaryawanController@import')->name('karyawan.import');
                Route::get('import/example', 'Admin\KaryawanController@importExample')->name('karyawan.import.example');
            });
            Route::prefix('admin')->group(function () {
                Route::get('/', 'Admin\AdminController@index')->name('admin.index');
                Route::get('/search', 'Admin\AdminController@search')->name('admin.search');
                Route::get('create', 'Admin\AdminController@create')->name('admin.create');
                Route::post('store', 'Admin\AdminController@store')->name('admin.store');
                Route::get('detail/{admin}', 'Admin\AdminController@show')->name('admin.detail');
                Route::get('edit/{admin}', 'Admin\AdminController@edit')->name('admin.edit');
                Route::put('update/{admin}', 'Admin\AdminController@update')->name('admin.update');
                Route::delete('delete/{admin}', 'Admin\AdminController@destroy')->name('admin.delete');
                Route::get('import', 'Admin\AdminController@import')->name('admin.import');
                Route::get('import/example', 'Admin\AdminController@importExample')->name('admin.import.example');
            });
        });
    });

    Route::group(['middleware' => ['Karyawan']], function () {
        Route::get('projects/{cost_center_id}', 'User\DocumentController@getProjects')->name('user.projects.list');
        Route::prefix('document')->group(function () {
            Route::get('/', 'User\DocumentController@index')->name('user.document.index');
            Route::get('create', 'User\DocumentController@create')->name('user.document.create');
            Route::post('store', 'User\DocumentController@store')->name('user.document.store');
            Route::get('/search', 'User\DocumentController@search')->name('user.document.search');
            Route::get('detail/{doc}', 'User\DocumentController@show')->name('user.document.detail');
            Route::post('ajukan/{doc}', 'User\DocumentController@ajukan')->name('user.document.ajukan');
            Route::post('settle/{doc}', 'User\DocumentController@settle')->name('user.document.settle');
        });
        Route::prefix('item')->group(function () {
            Route::post('create', 'User\ItemController@store')->name('user.item.store');
            Route::delete('delete/{item}', 'User\ItemController@delete')->name('user.item.delete');
        });
        Route::prefix('settle')->group(function () {
            Route::post('create', 'User\SettlementController@store')->name('user.settle.store');
            Route::delete('delete/{settle}', 'User\SettlementController@delete')->name('user.settle.delete');
        });
        Route::prefix('persetujuan')->group(function () {
            Route::get('/', 'User\PersetujuanController@persetujuan')->name('user.persetujuan.index');
            Route::get('detail/{doc}', 'User\PersetujuanController@show')->name('user.persetujuan.detail');
            Route::get('tolak/{doc}', 'User\PersetujuanController@tolakHtml')->name('user.persetujuan.tolak.html');
            Route::POST('tolak', 'User\PersetujuanController@tolak')->name('user.persetujuan.tolak');
            Route::POST('terima/{doc}', 'User\PersetujuanController@terima')->name('user.persetujuan.terima');
        });
    });

    Route::get('print/{dokumen_id}', 'Admin\DocumentController@docPrint')->name('document.print');
    
});

Auth::routes();
